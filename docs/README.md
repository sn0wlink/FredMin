# FredMin - Ver 1.0

A super small and lightweight Server Administration Tool.
This powerful little beast will happily sit on the most restricted servers
in-house, or on the cloud.

*Just remember to put it in a password protected folder.

## Installation

- Add a sudo user WWW-DATA
enter SU to access root User
enter visudo
then add user to the sudoers file:
www-data ALL=(ALL) NOPASSWD: ALL

- Dump it to a password protected folder

Have fun, and run with it!

## Screenshot

![image](https://raw.githubusercontent.com/sn0wlink/FredMin/master/screenshot.png)

## Contribution Rules
- Spaces not tabs (4 spaces)
- No Javascript
- Try to keep to 80 width
- There’s a way that works… and there’s the way it should be written. Nice code!

## Code of Conduct
Be excellent to each other!

## Contributors:
With great thanks to Ben Viperfang Bewick (for his help, support and general awesomeness)
