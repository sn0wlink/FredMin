<html>
        <head>
            <link rel="icon" href="favicon.ico" type="image/x-icon" />
            <?php include ('../config/config.php'); ?>
            <link rel="stylesheet" href="../config/style.php" type="text/css">

            <title>
                <?php echo "$controlname - $pagename"; ?>
            </title>
        </head>

<body>

<div class='header'>
	<a href='../index.php'><img class="logo" src='../images/header.png'></a>
	<p class='pagetitle'><?php echo $pagename ?>
</div>

<div class='menu'>
	<a href='../index.php'>Back to Homepage</a>
</div>

<div class='content'>

