/*

	David Collins-Cubit
	2018
	FredMin NiNite
	Control Panel for Web Developers

*/

body {
	background-color: #282728;
}

div.header {
	background-color: #4b4a4c;
	width: 100%;
	height: 90px;
	top: 0;
	left:0;
	position: fixed;
}

img.logo {
	opacity: 0;
	height: 90px;
	top: 0;
	left: 0;
	position: fixed;
}

p.pagetitle {
	color: #e5e5e5;
	position: absolute;
	width: 100%;
	top: 10px;
	text-align: center;
	font-family: arial, helvetica, dejavu;
	font-size: 20pt;
	font-weight: bold;
}

table {
    color: #e4e4e4;
}

p.menu-header {
	font-weight: bold;
	text-align: center;
	background-color: #4e4e4e;
	color: #e5e5e5;
	font-size: 10pt;
    font-family: arial, helvetica, dejavu;
}

div.menu {
	background-color: #5e5e5e;
	color: #e4e4e4;
    height: 100%;
	width: 160px;
	position: fixed;
	top: 0;
	left: 0;
	margin-top: 90px;
	padding: 10px;
	font-family: arial;
	font-size:10pt;
    font-family: arial, helvetica, dejavu;

}

a {
    color: #e5e5e5;
}

p.copyright {
	font-size: 10pt;
	color: #e5e5e5;
    font-family: arial, helvetica, dejavu;
	bottom: 0;
	left: 0;
	padding: 10px;
	width:160px;
	position: fixed;
}

img.onlinestatus {
	height: 9px;
	align: right;
}

input {
	height: 40px;
}

div.content {
	margin-top: 110px;
	margin-left: 200px;
	margin-right: 20px;
	margin-bottom: 50px;
    color: #e5e5e5;
	background-color: #5e5e5e;
	border:2px;
	padding: 20px;
	font-family: arial;
	text-align: left;
    font-family: arial, helvetica, dejavu;
}

div.catout {
	white-space: pre-line;
    font-family:courier;
    font-size:10pt;
}

img.icon {
	height: 30px;
}

div.icon {
	display: inline-block;
	padding-right: 40px;
	text-align: center;
	color: #e5e5e5;
}

a.icon {
	text-align: center;
	font-size: 11px;
	color: #e5e5e5;
	text-decoration: none;
}
